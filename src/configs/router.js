import React from "react";
import {createAppContainer, createStackNavigator} from "react-navigation";
import Card from "../components/Card/Card";
import CardDetail from "../components/CardDetail/CardDetail";

const AppNavigator = createStackNavigator({
    CardFeed: {
        screen: Card,
    },
    CardInfo: {
        screen: CardDetail,
    }
});

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;