import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    headerFinance: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',

    },
    textFinance: {
        fontSize: 20,
        color: 'rgb(255, 255, 255)',
    },

});

export default styles;