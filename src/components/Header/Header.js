import React from 'react';
import {Text, View} from 'react-native';

import styles from './styles';

class Header extends React.Component{

    render(){
        return(

            <View style = {styles.headerFinance}>
                <View>
                    {
                        <Text style = {styles.textFinance} >
                            {this.props.header}
                        </Text>

                    }
                </View>
            </View>

        );
    }
}

export default Header;
