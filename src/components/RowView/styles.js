import {StyleSheet} from "react-native";


const styles = StyleSheet.create({
    rowStyle: {
        flexDirection: 'row',
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        shadowOpacity: 0,

    },
    textOnLeft: {
        flex: 1,
        paddingBottom: 10,
        paddingTop: 10,
        shadowOpacity: 0,
    },
    textOnRight: {
        paddingBottom: 10,
        paddingTop: 10,
        alignItems: 'flex-end',
        shadowOpacity: 0,
    },
    leftTextStyle: {
        fontSize: 16,
        color: '#272526',

    },
    rightTextStyle: {
        fontSize: 16,
        color: '#272526',

    },
});

export default styles;