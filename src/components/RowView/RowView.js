import React from "react";
import {View, Text} from "react-native";
import styles from './styles';
const RowView = (props) => {
    return(
        <View style={styles.rowStyle}>

            <View style={styles.textOnLeft}>
                <Text style={styles.leftTextStyle}>{props.leftText}</Text>
            </View>
            <View style={styles.textOnRight}>
                <Text style={styles.rightTextStyle}>{props.rightText}</Text>
            </View>


        </View>
    );

};

export default RowView;