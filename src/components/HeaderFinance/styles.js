import {StyleSheet} from "react-native";
const styles = StyleSheet.create({
    headerStyling: {
        borderBottomWidth: 1,
        borderBottomColor: '#D8D8D8',
        paddingBottom: 15,
        paddingTop: 16, //Header'ın (örnek: Taşıt kredileri) üstündeki border'a mesafesi
        paddingLeft: 8,
    },
    headerText: {
        fontSize: 16,
        color: 'rgb(51, 51, 51)'
    }

});

export default styles;