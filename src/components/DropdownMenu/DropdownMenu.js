import React  from 'react';
import {TouchableOpacity} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import styles from './styles';
// MENÜLERİ EŞİT ARALIKLARLA AYIRAMADIM
//Menülere tıklandığında "listview is deprecated" uyarısı veriyor.
//Bunu önemseme çünkü asıl ekranda filtreleme ya da sıralamaya bastığında yeni bir ekran çıkacak.
class DropdownMenu extends React.Component{
    render(){
        return(
            <TouchableOpacity style={styles.menu}>
                <ModalDropdown
                    style={styles.dropdown_header}
                    textStyle={styles.dropdown_headertext}
                    dropdownStyle={styles.dropdown_dropdownStyle}
                    dropdownTextStyle={styles.dropdown_dropdownTextStyle}
                    defaultValue={this.props.value}
                    options={this.props.options}
                    //onSelect={this._dropdown_1_onSelect.bind(this)}
                />
            </TouchableOpacity>
        );
    }

    /* seçilen option'a göre sıralama yapılır
   _dropdown_1_onSelect(idx, value) {
       this._dropdown_5_idx = idx;
       /!*if (this._dropdown_5_idx != 0) {
           return false;
       }*!/
   }*/
}

export default DropdownMenu;