import styles from "./styles";
import {View, FlatList} from "react-native";
import HeaderFinance from "../HeaderFinance/HeaderFinance";
import FinanceCard from "../FinanceCard/FinanceCard";
import FooterForCard from "../FooterForCard/FooterForCard";
import React from "react";
import {List} from "react-native-elements";

const footerOptions=["Taksit Öde", "Ödeme Planı", "Tüm Bilgiler" ];


class Card extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            finance: [
                {
                    id: 1,
                    financeHeader: ["Taşıt Kredileri"],
                    financeNo_Branch: ["Finansman Numarası / Şube", "11/MERKEZ"],
                    financeAmount: ["Finansman Tutarı", 234430.98],
                    numberOfPayment: ["Taksit Sayısı", "8 / 12"],
                    dateOfPayment: ["Taksit Tarihi", "17.08.2018"],
                    amountOfPayment: ["Taksit Tutarı", 234430.98],
                },
                {
                    id: 2,
                    financeHeader: ["Ev Kredileri"],
                    financeNo_Branch: ["Finansman Numarası / Şube", "11/MERKEZ"],
                    financeAmount: ["Finansman Tutarı", 234430.98],
                    numberOfPayment: ["Taksit Sayısı", "8 / 12"],
                    dateOfPayment: ["Taksit Tarihi", "17.08.2018"],
                    amountOfPayment: ["Taksit Tutarı", 234430.98],
                },
                {
                    id: 3,
                    financeHeader: ["Okul Kredileri"],
                    financeNo_Branch: ["Finansman Numarası / Şube", "11/MERKEZ"],
                    financeAmount: ["Finansman Tutarı", 234430.98],
                    numberOfPayment: ["Taksit Sayısı", "8 / 12"],
                    dateOfPayment: ["Taksit Tarihi", "17.08.2018"],
                    amountOfPayment: ["Taksit Tutarı", 234430.98],
                },
            ],

        };

    }

    _renderItem = ({item}) => (
        <View style={styles.edges}>

                <View style={styles.headerRegion}>
                    <HeaderFinance header={item.financeHeader}/>
                </View>

            <View style={styles.cardPadding}>
                <FinanceCard key={item.id}
                             financeNo_Branch={item.financeNo_Branch}
                             financeAmount={item.financeAmount}
                             numberOfPayment={item.numberOfPayment}
                             dateOfPayment={item.dateOfPayment}
                             amountOfPayment={item.amountOfPayment}/>

            </View>
            <View style={styles.footerStyle}>

                <View style={styles.bordered}>

                    <FooterForCard value={footerOptions[0]}/>

                </View>

                <View style={styles.bordered}>

                    <FooterForCard value={footerOptions[1]}/>

                </View>

                <View style={styles.nonbordered}>

                    <FooterForCard value={footerOptions[2]}/>

                </View>

            </View>
        </View>
    );

    render(){
      return(
          <List containerStyle={{marginBottom: 80, borderTopWidth: 0, marginTop: 16}}>
              <FlatList
                  bounces={false}
                  data={this.state.finance}
                  keyExtractor={item => item.id.toString()}

                  renderItem={this._renderItem}

              />
          </List>


      );
  }
}
export default Card;