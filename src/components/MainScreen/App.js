import  React from 'react';
import {View, FlatList} from 'react-native';
import Header from "../Header/Header";
import styles from "./styles";
import DropdownMenu from "../DropdownMenu/DropdownMenu";
import Card from "../Card/Card";

const header = "Finansmanlar";

const menu=[
    {id:1, menuName: "Sırala", options: ["Tarihe Göre", "İşlem Miktarı Artan", "İşlem Miktarı Azalan", "Son işlem"]},
    {id:2, menuName: "Filtrele", options: ["Son 3 Ay", "Son 6 Ay", "Son 12 Ay"]},
    //{menuName: "Yeni Menu", options: ["Birinci menu", "ikinci menu", "Son menu"]},
    //{menuName: "Son Menu", options: ["Birinci menu", "ikinci menu", "Son menu"]},
];

export default class App extends React.Component {
    render() {
        return(
            <View style = {styles.container}>
                <Header header={header}/>
                <View style={styles.mainPage}>

                    <View style = {styles.menuStyle}>
                        {menu.map(item => <DropdownMenu key={item.id} value={item.menuName} options={item.options}/>)}
                    </View>

                    <View>
                        <Card/>
                    </View>

                </View>

            </View>

        );
    }
}



