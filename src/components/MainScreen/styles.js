import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: 'rgb(247, 64, 0)',
    },
    mainPage: {
        backgroundColor: 'rgb(255, 255, 255)',
        borderTopLeftRadius: 16,
        borderTopRightRadius: 16,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0,
        height: '85%',
    },
    menuStyle: {
        flexDirection: 'row',
    },

});

export default styles;