import React from "react";
import styles from "./styles";
import {View} from "react-native";
import RowView from "../RowView/RowView";

class FinanceCard extends React.Component{
    render() {
        return(
            <View>

                <View style={styles.lineStyle}>
                    <RowView leftText = {this.props.financeNo_Branch[0]} rightText = {this.props.financeNo_Branch[1]}/>
                </View>

                <View style={styles.lineStyle}>
                    <RowView leftText = {this.props.financeAmount[0]} rightText = {this.props.financeAmount[1]}/>
                </View>

                <View style={styles.lineStyle}>
                    <RowView leftText = {this.props.numberOfPayment[0]} rightText = {this.props.numberOfPayment[1]}/>
                </View>

                <View style={styles.lineStyle}>
                    <RowView leftText = {this.props.dateOfPayment[0]} rightText = {this.props.dateOfPayment[1]}/>
                </View>

                <View style={styles.lastLine}>
                    <RowView leftText = {this.props.amountOfPayment[0]} rightText = {this.props.amountOfPayment[1]}/>
                </View>

            </View>
        );
    }
}

export default FinanceCard;