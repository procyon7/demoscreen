import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 16,
        color: 'rgb(25, 166, 145)'
    }

});


export default styles;