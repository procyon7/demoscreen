/** @format */

import {AppRegistry} from 'react-native';
import App from './src/components/MainScreen/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
